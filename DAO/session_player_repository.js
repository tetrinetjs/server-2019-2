class SessionPlayerRepository {
    constructor(dao) {
        this.dao = dao
    }

    /*
    session_player_repository(
        ID_session
        ID_player
    );
    */

    /**
     * @param {Integer} ID_session
     * @param {Varchar(50)} ID_player
     * 
     */
    create(session) {
        const { ID_session, ID_player } = session
        return this.dao.run(
            'INSERT INTO session_player (ID_session, ID_player) VALUES (?,?)',
            [ID_session, ID_player])
    }

    
    updateID_player(SessionPlayer) {
        const { ID_session, ID_player } = SessionPlayer
        return this.dao.run(
            `UPDATE session_player SET ID_player = ? WHERE ID_player = ? AND ID_session = ? `,
            [ID_session, ID_player]
        )
    }

    updateID_session(SessionPlayer) {
        const { id_session, id_player } = SessionPlayer
        return this.dao.run(
            `UPDATE session_player SET ID_session = ? WHERE ID_player = ?`,
            [id_session, id_player]
        )
    }


    getById(player) {
        const { id_session, id_player } = player
        return this.dao.get(
            `SELECT * FROM session_player WHERE ID_player = ? and ID_session= ?`,
            [id_session, id_player])
    }

    getAll() {
        return this.dao.all(`SELECT * FROM session_player`)
    }

    delete(player) {
        const { id_session, id_player } = player
        return this.dao.run(
            `DELETE FROM session_player WHERE ID_player = ? and ID_session= ?`,
            [id_session, id_player]
        )
    }
    getAll() {
        return this.dao.all(`SELECT * FROM session_player`)
    }

}

module.exports = SessionPlayerRepository;
class ProjectRepository {
    constructor(dao) {
        this.dao = dao
    }

    createTable() {
        const sql = `CREATE TABLE IF NOT EXISTS session(
                    ID_session Integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                    name_session Varchar(50),
                    password_session Varchar(50),
                    player_count Integer DEFAULT 0,
                    active Boolean DEFAULT true);`

        return this.dao.run(sql)
    }

    create(name) {
        return this.dao.run(
            'INSERT INTO projects (name) VALUES (?)',
            [name])
    }
    update(project) {
        const { id, name } = project
        return this.dao.run(
            `UPDATE projects SET name = ? WHERE id = ?`,
            [name, id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM projects WHERE id = ?`,
            [id])
    }

    getAll() {
        return this.dao.all(`SELECT * FROM projects`)
    }

    delete(id) {
        return this.dao.run(
            `DELETE FROM projects WHERE id = ?`,
            [id]
        )
    }
    getAll() {
        return this.dao.all(`SELECT * FROM projects`)
    }
    
    getTasks(projectId) {
        return this.dao.all(
            `SELECT * FROM tasks WHERE projectId = ?`,
            [projectId])
    }

}

module.exports = ProjectRepository;
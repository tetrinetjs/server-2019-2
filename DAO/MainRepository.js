const fs = require('fs');
const metaCharset = 'utf-8'
const pathSchemaScript = './schema.sql'

class MainRepository {
    constructor(dao) {
        this.dao = dao
        this.schema = fs.readFileSync(pathSchemaScript, metaCharset );
    }

    createDataBase() {
        console.log(this.schema)
        const sql = this.schema;
        return this.dao.run(sql)
    }

}

module.exports = MainRepository;
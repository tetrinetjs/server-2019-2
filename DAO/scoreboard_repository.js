class ScoreboardRepository {
    constructor(dao) {
        this.dao = dao
    }

    /*
    scoreboard_repository(
        ID_session
        ID_player
        PlayerScore
    );
    */

    /**
     * @param {Integer} ID_session
     * @param {Varchar(50)} ID_player
     * @param {Integer} PlayerScore
     */
    create(session) {
        const { ID_session, ID_player, PlayerScore } = session
        return this.dao.run(
            'INSERT INTO scoreboard (ID_session, ID_player, PlayerScore) VALUES (?,?,?)',
            [ID_session, ID_player, PlayerScore])
    }

    /**
     * 
     * @param { ID_session, ID_player, PlayerScore} Score
     */
    updatePlayerScore(Score) {
        const { ID_session, ID_player, PlayerScore } = Score
        return this.dao.run(
            `UPDATE scoreboard SET PlayerScore = ? WHERE ID_player = ? AND ID_session = ? `,
            [ID_session, ID_player, PlayerScore]
        )
    }

    updateID_player(player) {
        const { ID_session, ID_player } = player
        return this.dao.run(
            `UPDATE scoreboard SET ID_player = ? WHERE ID_player = ? AND ID_session = ? `,
            [ID_session, ID_player]
        )
    }

    updateID_session(session) {
        const { id_session, id_player } = session
        return this.dao.run(
            `UPDATE scoreboard SET ID_session = ? WHERE ID_player = ?`,
            [id_session, id_player]
        )
    }


    getById(player) {
        const { id, id_session } = player
        return this.dao.get(
            `SELECT * FROM scoreboard WHERE ID_player = ? and ID_session= ?`,
            [id, id_session])
    }

    getAll() {
        return this.dao.all(`SELECT * FROM scoreboard`)
    }

    delete(player) {
        const { id, id_session } = player
        return this.dao.run(
            `DELETE FROM scoreboard WHERE ID_player = ? and ID_session= ?`,
            [id, id_session]
        )
    }
    getAll() {
        return this.dao.all(`SELECT * FROM scoreboard`)
    }

}

module.exports = ScoreboardRepository;
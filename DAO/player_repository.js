class PlayerRepository {
    constructor(dao) {
        this.dao = dao
    }

    create( player ) {
        const { name_player } = player
        return this.dao.run(
            'INSERT INTO player (name_player) VALUES (?)',
            [name_player])
    }

    updateLevel(player) {
        const { id, level_player } = player
        return this.dao.run(
            `UPDATE player SET level_player = ? WHERE ID_player = ?`,
            [level_player, id]
        )
    }
    
    updatePassword_player(player) {
        const { id, password_player } = player
        return this.dao.run(
            `UPDATE player SET password_player = ? WHERE ID_player = ?`,
            [password_player, id]
        )
    }

    updateMax_Score(player) {
        const { score, playerName  } = player
        return this.dao.run(
            `UPDATE player SET Max_Score = ? WHERE name_player = ?`,
            [score, playerName]
        )
    }


    getById(id) {
        return this.dao.get(
            `SELECT * FROM player WHERE ID_player = ?`,
            [id])
    }

    getByName(name_player) {
        return this.dao.get(
            `SELECT * FROM player WHERE name_player = ?`,
            [name_player])
    }

    getScoreByName(name_player) {
        return this.dao.get(
            `SELECT Max_Score FROM player WHERE name_player = ?`,
            [name_player])
    }

    exists(name_player) {
        return this.dao.get(
            `SELECT count(*) as value FROM player WHERE name_player = ?`,
            [name_player])
    }

    getAll() {
        return this.dao.all(`SELECT * FROM player order by MAX_Score limit 3`)
    }

    delete(id) {
        return this.dao.run(
            `DELETE FROM player WHERE ID_player = ?`,
            [id]
        )
    }
    getTopList() {
        return this.dao.all(`SELECT name_player, Max_Score FROM player where Max_Score is not null order by Max_Score desc limit 5`)
    }

}

module.exports = PlayerRepository;
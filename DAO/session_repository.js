class SessionRepository {
    constructor(dao) {
        this.dao = dao
    }
    /*session_repository(
    ID_session
    name_session
    password_session
    player_count
    active Boolean
    );*/
    create(session) {
        const { ID_session, name_session, password_session, player_count, active } = session;
        return this.dao.run(
            'INSERT INTO session (name_player, level_player, email_player, password_player) VALUES (?,?,?,?)',
            [ID_session, name_session, password_session, player_count, active]);
    }

    updateName_session(session) {
        const { name_session, ID_session } = session
        return this.dao.run(
            `UPDATE session SET name_session = ? WHERE ID_session = ?`,
            [name_session, ID_session]
        )
    }

    updatePassword_sesseion(session) {
        const { password_session, ID_session } = session
        return this.dao.run(
            `UPDATE session SET password_session = ? WHERE ID_session = ?`,
            [password_session, ID_session]
        )
    }

    updatePlayer_count(session) {
        const { player_count, ID_session } = session
        return this.dao.run(
            `UPDATE session SET player_count = ? WHERE ID_player = ?`,
            [player_count, ID_session]
        )
    }


    getById(ID_session) {
        return this.dao.get(
            `SELECT * FROM session WHERE ID_session = ?`,
            [ID_session])
    }

    getAll() {
        return this.dao.all(`SELECT * FROM session`)
    }

    delete(ID_session) {
        return this.dao.run(
            `DELETE FROM session WHERE ID_session = ?`,
            [ID_session]
        )
    }
    

}

module.exports = SessionRepository;
//@ts-check
const ClientRepo = require('../client_repository/ClientRepository');

/**
 * 
 * @param {*} app 
 * @param {ClientRepo} clientRepository 
 */
const chatManager = function(app, clientRepository){


	app.onReceive('pline', (params) => {
		params = String(params);
		console.log('pĺine');
		const seederId = parseInt( params.split(' ')[0] );
		const message = params.split(' ').slice(1).join(' ');

		//broadcast message to other clients
		const msg = `pline ${seederId} ${message}`;
		clientRepository.broadcastAllExcept(msg, seederId);
	});
	
	app.onReceive('plineact', (params) => {
		params = String(params);
		const seederId = parseInt( params.split(' ')[0] );
		const message = params.split(' ').slice(1).join(' ');
		const msg = `plineact ${seederId} ${message}`;
		clientRepository.broadcastAllExcept(msg, seederId);
	});

	app.onReceive('gmsg', (params) => {
		console.log(params);
		params = String(params);
		const msg = `gmsg ${params}`;
		clientRepository.broadcastAll(msg);
		//params = '<message>'
	})

}

module.exports = chatManager;
//@ts-check
const ClientRepo = require('../client_repository/ClientRepository');
const AppDAO      	 	= require('../DAO/dao');
const PlayerRepository  = require('../DAO/player_repository'); 
const dao               = new AppDAO('./database/tetrinet.db');
const SRCplayer         = new PlayerRepository(dao); 
/**
 * 
 * @param {*} app 
 * @param {ClientRepo} clientRepository 
 */

module.exports = function(app, clientRepository){
	
	app.onReceive('startgame', (params) => {
        let p = String(params);
        const playerId = parseInt( p.split(' ')[1] );
        const state = parseInt( p.split(' ')[0] );

        if(state == 1){
            clientRepository.broadcastAll('newgame 0 1 2 1 1 1 18 33333333333333555555555555552222222222222224444444444444466666666666666777777777777771111111111111111111111111111111111111111111111112222222222222222222234444444444444566666666666666678888889999999999 0 1');
        } else {
            clientRepository.broadcastAll('endgame');
        }
    });

    app.onReceive('f', (params) => {
        let p = String(params);
        const playerNum = parseInt( p.split(' ')[0] );
        const field = p.split(' ').slice(1).join(' ');
        const message = `f ${playerNum} ${field}`;
        clientRepository.broadcastAll(message); 
    });
    
    app.onReceive('lvl', (params) => {
        params = String(params);
        console.log(params)
        clientRepository.updateScore(params);
    });
    
    app.onReceive('sb', (params) => {
        const splitParams = String(params).split(' ');
        const sendernum = parseInt(splitParams[2]);
        clientRepository.broadcastAllExcept(`sb ${params}`, sendernum);
        console.log(params);
    });

    app.onReceive('playerlost', (params) => {
        clientRepository.broadcastAll(`playerlost ${params}`); 
        
        if(clientRepository.connectedClients.length = 1){
            writeScore(params);
            SRCplayer.getTopList()
					 .then((rows)=>{
						let winlist = 'winlist';
						
						for (let i = 0; i < rows.length; i++) {
							winlist = winlist.concat(` p${rows[i].name_player};${rows[i].Max_Score.toString()}`);
							
						}
						clientRepository.broadcastAll(winlist);
                        clientRepository.broadcastAll('endgame');
					 })
        }
        
    });
    //issue #3
    app.onReceive('playerwon', (params) => {
        const playerWinId = parseInt(params[0]);
        
        let winlist = "winlist tTourTheAbyss;922 pFregge;855 pelissa;475";

        clientRepository.broadcastAll(`playerwon ${playerWinId} ${winlist}`);
        writeScore(playerWinId);
        SRCplayer.getTopList()
        .then((rows)=>{
           let winlist = 'winlist';
           
           for (let i = 0; i < rows.length; i++) {
               winlist = winlist.concat(` p${rows[i].name_player};${rows[i].Max_Score.toString()}`);
               
           }
           clientRepository.broadcastAll(winlist);
           clientRepository.broadcastAll('endgame');
        })
        
    });

    
    app.onReceive('pause', (params) => {
        let p = String(params);
        const state = parseInt( p.split(' ')[0] );
        clientRepository.broadcastAll(`pause ${state}`);
        // pause e resume game
    });

    function writeScore(playerWinId) {
        clientRepository.connectedClients.forEach(element => {
           
            
            if(playerWinId === element.id ){
                const nameWin  = element.id;
                const scoreWin = element.name;

                SRCplayer.getScoreByName(nameWin)
                                .then((score)=>{
                                    if( score = scoreWin)
                                    var  newScore = {  score: scoreWin
                                                    ,playerName: nameWin }; 

                                    SRCplayer.updateMax_Score(newScore);
                                    //Esse código ainda está incompleto, pois tem um problema
                                    //pois ainda não foi encontrado ao certo, a onde o protocolo
                                    //envia o score local, do player
                                });
            }

        });

        
    }

}
//@ts-check
const ClientRepo  		= require('../client_repository/ClientRepository');
const AppDAO      	 	= require('../DAO/dao');
const PlayerRepository  = require('../DAO/player_repository'); 
const dao               = new AppDAO('./database/tetrinet.db');
const SRCplayer         = new PlayerRepository(dao); 
/**
 * 
 * @param {*} app 
 * @param {ClientRepo} clientRepository 
 */
const initConnectionManager = (app, clientRepository)=>{
	
	
	app.setOnConnectionListener((socket) => {
		if(!clientRepository.hasEmptySlot()){
			const reason = 'Server is full';
			const noConnectingMessage = `noconnecting ${reason}`;
			socket.writeTetrinetMessage(noConnectingMessage);
			return false;
		}

		return true;
	});

	app.onReceive('tetrisstart', (params, socketSender) => {
		params = String(params);
		const clientVersion = params.split(' ')[1];
		const playerName = params.split(' ')[0];
		 
		if(clientVersion === '1.13'){

			const newClient = clientRepository.addClient(playerName, socketSender);
						
			SRCplayer.getTopList()
					 .then((rows)=>{
						let winlist = 'winlist';
						
						for (let i = 0; i < rows.length; i++) {
							winlist = winlist.concat(` p${rows[i].name_player};${rows[i].Max_Score.toString()}`);
							
						}
						
						newClient.socket.writeTetrinetMessage(winlist);

					 })

			let loginResponse = `playernum ${newClient.id}`;
			newClient.socket.writeTetrinetMessage(loginResponse);
	
			//notify other players about connection
			const connectionMessage = `playerjoin ${newClient.id} ${newClient.name}`;
			clientRepository.broadcastAll(connectionMessage);

			//notify new client about others that are in the server
			clientRepository.connectedClients.forEach((client) => {
				const connectionMessage = `playerjoin ${client.id} ${client.name}`;
				newClient.socket.writeTetrinetMessage(connectionMessage);
			})
		}
	});

	//issue #3
	app.onReceive('team', (params, socketSender) => {	
		const splitParams = String(params).split(' ');
		const playerId = parseInt(splitParams[0]);
		const team = splitParams[1];

		clientRepository.addTeam(playerId, team);
		clientRepository.broadcastAll("team " + params);
	});	

	app.setOnSocketDisconnectCallback((socket) => {
		let disconnectedClient = clientRepository.removeClientBySocket(socket);
		//(issue: #5) verificar se o client é nulo (pode ja ter sido removido)
		if(disconnectedClient !== null){
			const message = `playerleave ${disconnectedClient.id}`;
			clientRepository.broadcastAll(message);
		}
	});

	
}

module.exports = initConnectionManager;
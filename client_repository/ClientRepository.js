const AppDAO      	 	= require('../DAO/dao');
const PlayerRepository  = require('../DAO/player_repository'); 
const dao               = new AppDAO('./database/tetrinet.db');
const SRCplayer         = new PlayerRepository(dao); 

module.exports =  class ClientRepository{
    constructor(){
        
        this.connectedClients = [];
        this.highScores = [];
        this.idQueue = [1,2,3,4,5,6];
    }

    
    addScore(playerName, score){
        const playerNameScore = {name: playerName, score: score}
        SRCplayer.getScoreByName(playerNameScore.name)
                 .then((score)=>{
                    if(playerNameScore.score > score){
                        SRCplayer.updateMax_Score(playerNameScore.score)
                                 .then(console.log('Novo Alto recorde!'))
                    }
        })

        
    }

    updateScore(params){
        let score = {
            id : params.split(' ')[0],
            value : params.split(' ')[1] 
        }

        if( this.highScores.length < score.id ){
            this.highScores.push(score);
        }else{
            this.highScores[score.id] = score;
        }
        
    }

    addClient(playerName, socket){
        
        var client = {
            id: this.idQueue.shift(),
            name: playerName,
            socket: socket,
            team: ""//issue #3
        }

        if(this.connectedClients.length > 0){
            this.connectedClients.forEach((clientItem) => {
                if( clientItem.name === playerName ){
                    var endName = client.id;
                    client.name = playerName+endName.toString();
                    console.log('Usuário já existente, nickName alterado para: '+client.name+';');
                }
            });
        }

        SRCplayer.exists(playerName)
                 .then((value)=>{
                        if(value.value == 0){
                            SRCplayer.create({'name_player':playerName})
                                    .then(console.log('Novo Player Criado!: '+playerName))
                        }
        });

        this.connectedClients.push(client);

        return client;
    }

    //issue #3
    addTeam(playerId, team){
        if(this.connectedClients.length > 0){
            this.connectedClients.forEach((clientItem) => {
                if( clientItem.id === playerId ){
                    clientItem.team = team;
                    console.log('jogador entrou para time:' + clientItem.team);
                }
            });
        }
    }    

    removeClientBySocket(socket){
        let removedClient = null;
        this.connectedClients = this.connectedClients.filter((client) => {
            if(socket === client.socket){
                this.idQueue.push(client.id);
                removedClient = client;
            }else{
                return client;
            }
        });

        return removedClient;
    }

    broadcastAll(message){
        this.connectedClients.forEach((client) => client.socket.writeTetrinetMessage(message))
    }

    broadcastAllExcept(message, id){
        this.connectedClients.forEach((client) => {
            if( client.id !== id ){
                client.socket.writeTetrinetMessage(message)
            }
        });
    }

    hasEmptySlot(){
        return this.idQueue.length > 0;
    }




}
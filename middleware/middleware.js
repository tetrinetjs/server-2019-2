//@ts-check
const Socket = require('net').Socket;
const net = require('net');
const decodeLogin = require('./protocol/decodeLogin');
const tetrinetProtocolIO = require('./protocol/IO');
const isLoginMessage = require('./protocol/isLoginMessage');

var defaultWrite = net.Socket.prototype.write;
//net.Socket.prototype.write = function(data, cb){
//    defaultWrite(tetrinetProtocolIO.encode(data), cb)
//} 

class TetrijsSocket{
    /**
     * 
     * @param {Socket} socket 
     */
    constructor(socket){
        this.socket = socket;
    }

    /**
     * 
     * @param {string} message 
     */
    writeTetrinetMessage(message){
        this.socket.write(tetrinetProtocolIO.encode(message));
    }
}




const App = () => {

    let commandsCallbacksObj = [];
    /**
     * 
     * @param {(socket: TetrijsSocket) => void} callback 
     */
    let onSocketConnectCallback;
    let onSocketDisconnectCallback;

    /**
     * 
     * @param {String} command 
     * @param {(params: String, socket: TetrijsSocket)} callback 
     */
    const _on = (command, callback) => {
        commandsCallbacksObj.push({command: command, callback: callback});
    }


    const server = net.createServer((socket) => {
        //(issue: #5) verificação de conexão com cliente - se cliente perder conexão com servidor, será identificado e desconectado do servidor em 5s
        //erro de conexão executa: tSocket.socket.on('close',[..]
        socket.setKeepAlive(true, 5000);

        let tSocket = new TetrijsSocket(socket);
       
        let connectionApproved = true;
        if(onSocketConnectCallback){
            connectionApproved = onSocketConnectCallback(tSocket);
        }

        if(!connectionApproved){
            return;
        }

        tSocket.socket.on('data', (data) => {
            
            let message = tetrinetProtocolIO.read(data);
            
            if(isLoginMessage(message)){
                message = decodeLogin( message );
            }
     
            let splitedMessage = message.split(' ');
            const command = splitedMessage[0];
            const params = splitedMessage.slice(1).join(' ');
            let commandCallbackObj = commandsCallbacksObj.find(x => x.command === command);
            if(commandCallbackObj){
                commandCallbackObj.callback(params, tSocket);
            }
            

        });

        //(issue: #5) client disconecta com erro, exemplo: erro de conexão
        tSocket.socket.on('close', () => {
            onSocketDisconnectCallback(tSocket);
        });

        // client desconecta normlamente
        tSocket.socket.on('end', () => {
            onSocketDisconnectCallback(tSocket);
        });

        tSocket.socket.on('error', (err) => {
            var json = JSON.parse(JSON.stringify(err));//converter erro para json
            if(json['errno'] !== "ECONNRESET"){//não mostrar erro no server quando client perde conexão
                console.log(err);
            }            
        });
    });
   
    return {
        onReceive: _on,
        server: server,
        
        /**
         * 
         * @param {(socket: TetrijsSocket) => boolean} callback 
         */
        setOnConnectionListener: function(callback){
            onSocketConnectCallback = callback;
        },
        /**
         * 
         * @param {(socket: TetrijsSocket) => void} callback 
         */
        setOnSocketDisconnectCallback: function(callback){
            onSocketDisconnectCallback = callback;
        }
    }
}

module.exports = App;
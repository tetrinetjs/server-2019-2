//@ts-check

/**
 * 
 * @param {Array<Number>} dec 
 * @param {Boolean} tetrifast
 * @returns {Array<Number>} hashpattern 
 */
function findHashPattern(dec, tetrifast)
{
  // the first characters from the decoded string
  let data = (tetrifast ? "tetrifaste" : "tetrisstar").split('');


  // compute the full hash
  let h = [];
  for (let i = 0; i < data.length; i++)
  {
    h[i] = ((data[i].charCodeAt(0) + dec[i]) % 255) ^ dec[i + 1];
  }

  let hLength = 5;

  for (let i = 5; i == hLength && i > 0; i--){
    for (let j = 0; j < data.length - hLength; j++){
      if (h[j] != h[j+hLength])
         hLength--;
    }
  }

  return h.slice(0, hLength);
}

/**
 * 
 * @param {String} loginEncodedString 
 * @returns {String}
 */
function decodeLogin(loginEncodedString)
{
  // parse the hex values from the init string
  let dec = [];

  for (var i = 0; i < loginEncodedString.length / 2; i++)
  {
    dec[i] = parseInt(loginEncodedString.substring(i * 2, i * 2 + 2), 16);
  }

   
  //const hashPattern = findHashPattern(dec, false);
  // find the h from enconding 
  let pattern = findHashPattern(dec, false);
        

  // decode the string
  var decodedString = "";
  for (var i = 1; i < dec.length; i++){
    decodedString = decodedString + String.fromCharCode( (((dec[i] ^ pattern[ (i - 1) % pattern.length]) + 255 - dec[i-1] ) % 255) );
  }

 
  return decodedString.replace(String.fromCharCode(0), String.fromCharCode(255));
 
}

module.exports = decodeLogin;
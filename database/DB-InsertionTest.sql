Insert into session (name_session, password_session, player_count, active) values
("Joguinho1",00000, 3, "false"),
("joguinho2",00000, 3, "false"),
("joguinho3",00000, 4, "true");

Insert into player (name_player, level_player, email_player,password_player, Max_Score)
VALUES
("Thiago", 5, "thiago@teste", "thiago",1600),
("Matheus", 10, "matheus@teste", "matheus", 2000),
("Breno", 8, "breno@teste", "breno", 1900),
("Ian", 6, "ian@teste", "ian", 1500),
("Frank", 4, "frank@este", "frank", 1300),
("Carol", 6, "carol@teste", "carol", 1500);

Insert into session_player (ID_session, ID_player)
VALUES
(1, 1),
(1, 3),
(1, 5),
(2, 2),
(2, 4),
(2, 6),
(3, 3),
(3, 4),
(3, 5);


Insert into scoreboard (ID_session, ID_player, PlayerScore)
VALUES
(1, 1, 1600),
(1, 3, 1900),
(1, 5, 1300),
(2, 2, 2000),
(2, 4, 1500),
(2, 6, 1500),
(3, 3, 2000),
(3, 4, 1700),
(3, 5, 1400);


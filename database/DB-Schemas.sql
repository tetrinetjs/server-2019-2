PRAGMA foreign_keys = ON;
.header ON
.mode colum

CREATE TABLE IF NOT EXISTS session(
 
    ID_session Integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    name_session Varchar(50),
    password_session Varchar(50),
    player_count Integer DEFAULT 0,
    active Boolean DEFAULT true
);

CREATE TABLE IF NOT EXISTS player(
    ID_player Integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    name_player Varchar(50) UNIQUE,
    level_player INTEGER CHECK(level_player > 0),
    email_player Varchar(100),
    password_player varchar(50),
    Max_Score Float
);

CREATE TABLE IF NOT EXISTS session_player(
    ID_session Integer NOT NULL,
    ID_player Integer NOT NULL,
    PRIMARY KEY(ID_session, ID_player),
    FOREIGN KEY(ID_player) REFERENCES player(ID_player)
            ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY(ID_session) REFERENCES session(ID_session)
            ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS scoreboard(
    ID_session Integer NOT NULL,
    ID_player Integer NOT NULL,
    PlayerScore FLOAT DEFAULT 0,
    PRIMARY KEY(ID_session, ID_player),
    FOREIGN KEY(ID_player) REFERENCES player(ID_player)
            ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY(ID_session) REFERENCES session(ID_session)
            ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS scoreboard(
    ID_session Integer NOT NULL,
    ID_player Integer NOT NULL,
    PlayerScore FLOAT DEFAULT 0,
    PRIMARY KEY(ID_session, ID_player)
);

CREATE VIEW see_globalscore AS
Select scoreboard.ID_player, name_player, level_player, PlayerScore
FROM scoreboard INNER JOIN player 
        on scoreboard.ID_player = player.ID_player
GROUP BY scoreboard.ID_player
ORDER BY PlayerScore DESC;


CREATE VIEW see_sessionscore AS
Select ID_session, scoreboard.ID_player, name_player, level_player, PlayerScore
FROM scoreboard INNER JOIN player 
        on scoreboard.ID_player = player.ID_player
ORDER BY PlayerScore DESC;
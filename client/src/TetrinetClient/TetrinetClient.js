//@ts-check
const net = require('net');
const Protocol = require('../protocol/Protocol').Protocol;

module.exports.TetrinetClient = class TetrinetClient {
    constructor() {
        this.protocol = new Protocol();
        this.client = null;
    }

    connect({username, host = '127.0.0.1', port = 31457}) {
        const connectionClient = net.createConnection({ port: port, host: host }, () => {
            var nick = username;
            var _version = "1.13";
            var ip = host.split('.');
            var t = false;
            var strConexao = this.protocol.encodeLogin(nick,_version,ip,t);
            connectionClient.write(strConexao);
        });
        this.client = connectionClient;
    }

    /**
     * 
     * @param {string} message 
     */
    sendMessage(message) {
        const encodedMessage = this.protocol.encodeMessage(message);
        this.client.write(encodedMessage);
    }

    /**
     * 
     * @param {(message: string) => void} callback 
     */
    onReceiveData(callback) {
        this.client.on('data', (data) => {
            const decodedMessage = this.protocol.decodeMessage(data.toString());
            callback(decodedMessage);
        });
    }

    /**
     * 
     * @param {() => void} callback 
     */
    closeConnection(callback) {
        this.client.end(callback)
    }
    
    /**
     * 
     * @param {(err: any) => void} callback 
     */
    onError(callback) {
        this.client.on('error', (err) => {
            callback(err);
        })
    }

    /**
     * 
     * @param {() => void} callback 
     */
    onEnd(callback) {
        this.client.on('end', () => {
            callback();
        })
    }
};

/*/
Envia ao servidor a informação de que o jogador local deu início/fim ao jogo.
O servidor só responde se o jogador local que está enviando é o operador do canal.
startgame <state> <playernum>
/*/

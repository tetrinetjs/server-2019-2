# Equipe de desenvolvimento do cliente

## Objetivos:
* Desenvolver a biblioteca cliente TCP/tetrinet em nodejs

## Grupo 4 - membros
* PEDRO HENRIQUE FERNANDES DOS REIS (líder)
* MATHEWS ARANTES PEREIRA
* RODRIGO DE OLIVEIRA GUIMARAES
* LUCAS DE OLIVEIRA NAVES
* TIAGO MOURA DE FARIA

## Tarefas da sprint 1 (25/03 a 31/03):
* Criar protótipo inicial
* Criar snippets de modelos de bibliotecas nodejs


###### Executar

* Para testar a aplicação, basta executar `node index.js`
//@ts-check
const port = 31457;
const host = '0.0.0.0';

const app = require('./middleware/middleware')();
const ClientRepository = require('./client_repository/ClientRepository');
const chatManager = require('./managers/chat_manager');
const connectionManager = require('./managers/connection_manager');
const gameManager = require('./managers/game_manager');

// initializing client repository
const clientRepository = new ClientRepository();

// initializing managers
chatManager(app, clientRepository);
connectionManager(app, clientRepository);
gameManager(app, clientRepository);



app.server.listen(port, host, () => console.log(`UFG TetriNET Server listening on ${host}:${port}`));
//bhh